# RPG Template

A little game template I used to conduct lessons for my students.  
Features WASD movement, camera, tilemap parsing, collisions, and a tiny interaction with an NPC.
